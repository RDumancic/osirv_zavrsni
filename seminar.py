import numpy as  np
import cv2
import matplotlib.pyplot as plt

def gaussian_noise(img, mi, sigma):
    output = img.astype(np.float32)
    noise = np.random.normal(mi, sigma, img.shape)
    output = output + noise
    output[output<0] = 0
    output[output>255] = 255
    return output.astype(np.uint8)

def salt_n_pepper_noise(img, percent):
    output = img.astype(np.float32)
    limit = ((float(percent)/2.0)/100.0) * 255.0
    noise = np.random.uniform(0,255, img.shape)
    output[noise<limit] = 0
    output[noise>(255-limit)] = 255
    output[output>255] = 255
    output[output<0] = 0
    return output.astype(np.uint8)

def uniform_noise(img, low, high):
    output = img.astype(np.float32)
    noise = np.random.uniform(low, high, img.shape)
    output = output + noise
    output[output>255] = 255
    output[output<0] = 0
    return output.astype(np.uint8)

image = cv2.imread("images/test.jpg", 0)
corrupt_images = []
cv2.imwrite("Original.png", image)

sigmas = [5, 10, 20, 40, 60]
borders = [(-20, 20), (-40, 40), (-60, 60)]
percentages = [5, 10, 15, 20]

for sigma in sigmas:
    corrupt_images.append((gaussian_noise(image, 0, sigma), "gnoise_" + str(sigma)))
    cv2.imwrite("gaussian_sigma" + str(sigma) + ".png", gaussian_noise(image, 0, sigma))

for percentage in percentages:
    corrupt_images.append((salt_n_pepper_noise(image, percentage), "snp_" + str(percentage)))
    cv2.imwrite("saltpepper_" + str(percentage) + ".png", salt_n_pepper_noise(image, percentage))

for (low, high) in borders:
    corrupt_images.append((uniform_noise(image, low, high), "uni_" + str(low) + "_" + str(high)))
    cv2.imwrite("uniform_" + str(low) + "to" + str(high) + ".png", uniform_noise(image, low, high))

medians = [1, 3, 5]
kernels = [(3,3), (5,5)]
sizes = [50, 100, 150]

for (image,name) in corrupt_images:
    for size in sizes:
        cv2.imwrite("Bilateral_" + str(size) + "_" + name + ".png", cv2.bilateralFilter(image,9,size,size))
    for median in medians:
        cv2.imwrite("MedianB_" + str(median) + "_" + name + ".png", cv2.medianBlur(image, median))
    for kernel in kernels:
        cv2.imwrite("Avg" + str(kernel) + "_" + name + ".png", cv2.blur(image, kernel))
        for sigma in sigmas:
            cv2.imwrite("GaussB_" + str(kernel) + "_" + name + ".png", cv2.GaussianBlur(image, kernel, sigma))
            